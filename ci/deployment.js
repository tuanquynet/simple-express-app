var path, node_ssh, ssh;
var async = require('async');

path = require('path');
node_ssh = require('node-ssh');
ssh = new node_ssh();
var CWD = process.cwd();
var HOME = process.env.HOME;

ssh.connect({
  host: 'demo.tuanquynet.click',
  username: 'pi',
  privateKey: HOME + '/.ssh/id_rsa'
}).then(function() {
	async.series([
		function (callback) {
			ssh.exec('ls', ['/home/pi'], { cwd: '/home/pi', stream: 'stdout' })
				.then(function (result) {
					console.log('execCommand');
					console.log(arguments);
					callback();
				}, function(error) {
					console.log("Something's wrong");
					console.log(error);
					callback();
				});
		},
		function (callback) {
			ssh.exec('git pull', [''], { cwd: '/home/pi/apps/simple-express-app', stream: 'stdout' })
				.then(function (result) {
					console.log('execCommand');
					console.log(arguments);
					callback();
				}, function(error) {
					console.log("Something's wrong");
					console.log(error);
					callback();
				});
		},
		function (callback) {
			//simple-app should be running on the server
			ssh.exec('pm2 stop simple-app && pm2 start simple-app', [], { cwd: '/home/pi/apps/simple-express-app', stream: 'stdout' })
				.then(function (result) {
					console.log('execCommand: "pm2 start index.js" ');
					console.log(arguments);
					callback();
				}, function(error) {
					console.log("Something's wrong");
					console.log(error);
					callback();
				});
		}
	], function(err) {
		console.log('error');
		console.log(err);
		process.exit();
	});
}).catch(function (err) {
	console.log('error');
	console.log(err);
	process.exit();
});

setInterval(function () {}, 500);
