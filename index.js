/*global process*/
'use strict';
var express = require('express');
var app = express();
var cors = require('cors');

/*Allow CORS*/
app.use(cors());

app.all('/dbspips/common/getUserBasicInfo', function (req, res, next) {
	var data = {header: "", data: {}};
	res.send(JSON.stringify(data));
});

app.use(express.static('./'));

app.get('/', function (req, res) {
  res.send('Hello World!');
});
// :id(^[a-z]{0,10}$)
app.get('/:lang([a-z]{2})/contact/', function (req, res) {
	console.log(req.params);
	console.log(req.query);
  res.send('contact');
});

app.get('/keystone', function (req, res) {
  res.send('keystone');
});

// var host = process.env.host;
var port = process.env.PORT || 9002;

app.listen(port, function () {
  console.log('Example app listening at http://%s:%s', port);
});
